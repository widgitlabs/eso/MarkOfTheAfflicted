# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.1] - 2023-04-09

### Fixed

- Naming consistency so git installations don't break

## [1.2.0] - 2023-04-08

### Changed

- API version

### Removed

- Bundled dependencies

### Fixed

- Settings handler incorrectly loading defaults

## [1.1.1] - 2020-08-30

### Fixed

- Missing fonts
- Vampire timers

## [1.1.0] - 2020-08-18

### Updated

- Brought codebase up to date

## [1.0.3] - 2018-08-17

### Updated

- API version

## [1.0.2] - 2017-06-01

### Fixed

- Minor bugfix

## [1.0.1] - 2017-05-30

### Fixed

- Minor bugfix

## [1.0.0] - 2017-05-28

### Added

- Initial release
